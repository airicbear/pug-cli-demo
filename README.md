# Pug CLI Demo

[airicbear/pug-cli-demo](https://github.com/airicbear/pug-cli-demo/)

## Install Pug
```
npm install pug-cli -g
```

## Convert to HTML

```
pug -w ./ -o ./html -P
```
